﻿/*********************************
 * Created by: David Giraldo
 * Date Created: 11/03/2021
 * Last Updated by: David Giraldo
 * Last Updated: 11/03/2021
 *
 * Files handles the links for the localisation APIs.
 * ******************************************/
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace ELS_Localise
{
    /// <summary>
    /// This is the type of connection To be Used
    /// </summary>
    public enum ConnectionType
    {
        Development,
        Release
    }

    public enum RequestType
    {
        GET,
        POST,
        PUT
    }

    [System.Serializable]
    public enum ThemeTextDetails
    {
        Title,
        SubTitle,
        Content,
        ExtraContent
    }

    [System.Serializable]
    public enum ImageThemeColours
    {
        PrimaryColour,
        SecundaryColour,
        ExtraColour1,
        ExtraColour2
}

    [System.Serializable]
    public struct MainThemeDetails
    {
        public Color32 PrimaryColour;
        public Color32 SecundaryColour;
        public Color32 WarnColour;
        public Color32 ExtraColour2;
    }


    [System.Serializable]
    public class FontDetails
    {
        public ThemeTextDetails ThemeDetails;
        public TMP_FontAsset Font;
        public Color32 TextColour;
        public Color32 TextAlternativeColour;
    }

    [System.Serializable]
    public class LanguageThemeDetails
    {
        public List<FontDetails> FontsDetail;
        public MainThemeDetails MainThemeColors;
    }

    [System.Serializable]
    public class Translation
    {
        public string key;
        public string text;
    }

    [System.Serializable]
    public class VoiceOver
    {
        public string key;
        public string url;
    }

    [System.Serializable]
    public class LanguageDetails
    {
        public string language;
        public string direction;
        public bool join;
        public List<Translation> translation;
        public List<VoiceOver> voice_over;
    }

    [System.Serializable]
    public class LanguagesAvailable
    {
        public List<LanguageDetails> languages;
    }

    #region Languages
    /// <summary>
    /// List of all possible Languages.
    /// </summary>
    public enum Languages
    {
        en_GB,
        en_AU,
        en_BZ,
        en_CA,
        en_CB,
        en_IE,
        en_JM,
        en_NZ,
        en_PH,
        en_TT,
        en_US,
        en_ZA,
        en_ZW,
        Cy_az_AZ,
        Cy_sr_SP,
        Cy_uz_UZ,
        Lt_az_AZ,
        Lt_sr_SP,
        Lt_uz_UZ,
        aa,
        ab,
        ae,
        af,
        af_ZA,
        ak,
        am,
        an,
        ar,
        ar_AE,
        ar_BH,
        ar_DZ,
        ar_EG,
        ar_IQ,
        ar_JO,
        ar_KW,
        ar_LB,
        ar_LY,
        ar_MA,
        ar_OM,
        ar_QA,
        ar_SA,
        ar_SY,
        ar_TN,
        ar_YE,
        av,
        ay,
        az,
        ba,
        be,
        be_BY,
        bg,
        bg_BG,
        bh,
        bi,
        bm,
        bn,
        bo,
        br,
        bs,
        ca,
        ca_ES,
        ce,
        ch,
        co,
        cr,
        cs,
        cs_CZ,
        cu,
        cv,
        cy,
        da,
        da_DK,
        de,
        de_AT,
        de_CH,
        de_DE,
        de_LI,
        de_LU,
        div_MV,
        dv,
        dz,
        ee,
        el,
        el_GR,
        en,
        eo,
        es,
        es_AR,
        es_BO,
        es_CL,
        es_CO,
        es_CR,
        es_DO,
        es_EC,
        es_ES,
        es_GT,
        es_HN,
        es_MX,
        es_NI,
        es_PA,
        es_PE,
        es_PR,
        es_PY,
        es_SV,
        es_UY,
        es_VE,
        et,
        et_EE,
        eu,
        eu_ES,
        fa,
        fa_IR,
        ff,
        fi,
        fi_FI,
        fj,
        fo,
        fo_FO,
        fr,
        fr_BE,
        fr_CA,
        fr_CH,
        fr_FR,
        fr_LU,
        fr_MC,
        fy,
        ga,
        gd,
        gl,
        gl_ES,
        gn,
        gu,
        gu_IN,
        gv,
        ha,
        he,
        he_IL,
        hi,
        hi_IN,
        ho,
        hr,
        hr_HR,
        ht,
        hu,
        hu_HU,
        hy,
        hy_AM,
        hz,
        ia,
        id,
        id_ID,
        ie,
        ig,
        ii,
        ik,
        io,
        is_IS,
        it,
        it_CH,
        it_IT,
        iu,
        ja,
        ja_JP,
        jv,
        ka,
        ka_GE,
        kg,
        ki,
        kj,
        kk,
        kk_KZ,
        kl,
        km,
        kn,
        kn_IN,
        ko,
        ko_KR,
        kr,
        ks,
        ku,
        kv,
        kw,
        ky,
        ky_KZ,
        la,
        lb,
        lg,
        li,
        ln,
        lo,
        lt,
        lt_LT,
        lu,
        lv,
        lv_LV,
        mg,
        mh,
        mi,
        mk,
        mk_MK,
        ml,
        mn,
        mn_MN,
        mr,
        mr_IN,
        ms,
        ms_BN,
        ms_MY,
        mt,
        my,
        na,
        nb,
        nb_NO,
        nd,
        ne,
        ng,
        nl,
        nl_BE,
        nl_NL,
        nn,
        nn_NO,
        no,
        nr,
        nv,
        ny,
        oc,
        oj,
        om,
        or,
        os,
        pa,
        pa_IN,
        pi,
        pl,
        pl_PL,
        ps,
        pt,
        pt_BR,
        pt_PT,
        qu,
        rm,
        rn,
        ro,
        ro_RO,
        ru,
        ru_RU,
        rw,
        sa,
        sa_IN,
        sc,
        sd,
        se,
        sg,
        si,
        sk,
        sk_SK,
        sl,
        sl_SI,
        sm,
        sn,
        so,
        sq,
        sq_AL,
        sr,
        ss,
        st,
        su,
        sv,
        sv_FI,
        sv_SE,
        sw,
        sw_KE,
        ta,
        ta_IN,
        te,
        te_IN,
        tg,
        th,
        th_TH,
        ti,
        tk,
        tl,
        tn,
        to,
        tr,
        tr_TR,
        ts,
        tt,
        tt_RU,
        tw,
        ty,
        ug,
        uk,
        uk_UA,
        ur,
        ur_PK,
        uz,
        ve,
        vi,
        vi_VN,
        vo,
        wa,
        wo,
        xh,
        yi,
        yo,
        za,
        zh,
        zh_CHS,
        zh_CHT,
        zh_CN,
        zh_HK,
        zh_MO,
        zh_SG,
        zh_TW,
        zu
    }

    #endregion
}
