﻿/*********************************
 * Created by: David Giraldo
 * Date Created: 20/03/2021
 * Last Updated by: David Giraldo
 * Last Updated: 24/03/2021
 *
 * Files handles the fixing of the arabic letters.
 * ******************************************/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using RTLTMPro;

public class ELS_arabicFixer : MonoBehaviour
{
    [SerializeField] protected bool _preserveNumbers;
    [SerializeField] protected bool _farsi = true;
    [SerializeField] protected bool _fixTags = true;
    [SerializeField] protected bool _forceFix;
    [SerializeField] private ELS_TextBase _els_Text;

    protected readonly FastStringBuilder finalText = new FastStringBuilder(RTLSupport.DefaultBufferSize);

    /// <summary>
	/// Gets the ELS_TextBase component.
	/// </summary>
    protected virtual void OnValidate()
    {
        _els_Text = GetComponent<ELS_TextBase>();
    }

    /// <summary>
	/// Returns the fixed text.
	/// </summary>
	/// <param name="input"></param>
	/// <returns></returns>
    public string GetFixedText(string input)
    {
        if (string.IsNullOrEmpty(input))
            return input;

        finalText.Clear();
        RTLSupport.FixRTL(input, finalText, _farsi, _fixTags, _preserveNumbers);
        finalText.Reverse();

        return finalText.ToString();
    }
}
