/* ***********************************
 * Created by: Paulo Renan
 * Created: 14/07/2021, 14:06
 * Last Updated by: Paulo Renan
 * Last Updated: 14/07/2021, 14:06
 * 
 * Use this instead of MonoBehaviour to make your object have a persistant unique ID
 * **********************************/
/// See also <see cref="ELS_TextBase"/>
/* * * * *
 * Unity Unique ID
 * ----------------
 * 
 * This component tries to solve the problem of generating a GUID for an object
 * that persists during development and at runtime and prevents any duplicates
 * of the ID. That means if an object is copied, cloned or instantiated the new
 * object should get a new ID. This also prevents accidental changes of an assigned
 * ID through "revert to prefab" or "apply". This is done by two static dictionaries
 * which track both, the ID as well as the components. Even after an assembly reload
 * all currently loaded objects / prefabs get immediately registrated again.
 * 
 * I've done several test to check if the ID correctly persists and it seems to work
 * in all cases. Though if you found a reproducible bug feel free to file an issue
 * https://github.com/Bunny83/UUID/issues
 * 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2012-2017 Markus Göbel (Bunny83)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 * 
 * * * * */
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Use this if you need a monobehaviour with a unique ID that is serialized with the object (persists)
/// </summary>
#if UNITY_EDITOR
//[ExecuteInEditMode]
#endif
public abstract class MonoBehaviour_UUID : MonoBehaviour, ISerializationCallbackReceiver
{
    #region Static serialization and database
    static Dictionary<MonoBehaviour_UUID, string> m_ObjToUUID = new Dictionary<MonoBehaviour_UUID, string>();
    static Dictionary<string, MonoBehaviour_UUID> m_UUIDtoObj = new Dictionary<string, MonoBehaviour_UUID>();

    private static void RegisterUUID(MonoBehaviour_UUID aID)
    {
        string UID;
        if (m_ObjToUUID.TryGetValue(aID, out UID))
        {
            //Debug.Log("found object instance, update ID");
            // found object instance, update ID
            aID.m_UUID = UID;
            aID.m_IDBackup = aID.m_UUID;
            if (!m_UUIDtoObj.ContainsKey(UID))
                m_UUIDtoObj.Add(UID, aID);
            return;
        }

        if (string.IsNullOrEmpty(aID.m_UUID))
        {
            //Debug.Log("No ID yet, generate a new one.");
            // No ID yet, generate a new one.
            aID.m_UUID = System.Guid.NewGuid().ToString();
            aID.m_IDBackup = aID.m_UUID;
            m_UUIDtoObj.Add(aID.m_UUID, aID);
            m_ObjToUUID.Add(aID, aID.m_UUID);
            return;
        }

        MonoBehaviour_UUID tmp;
        if (!m_UUIDtoObj.TryGetValue(aID.m_UUID, out tmp))
        {
            //Debug.Log("ID not known to the DB, so just register it");
            // ID not known to the DB, so just register it
            m_UUIDtoObj.Add(aID.m_UUID, aID);
            m_ObjToUUID.Add(aID, aID.m_UUID);
            return;
        }
        if (tmp == aID)
        {
            // DB inconsistency
            m_ObjToUUID.Add(aID, aID.m_UUID);
            return;
        }
        if (tmp == null)
        {
            //Debug.Log("object in DB got destroyed, replace with new");
            // object in DB got destroyed, replace with new
            m_UUIDtoObj[aID.m_UUID] = aID;
            m_ObjToUUID.Add(aID, aID.m_UUID);
            return;
        }
        //Debug.Log("we got a duplicate, generate new ID");
        // we got a duplicate, generate new ID
        aID.m_UUID = System.Guid.NewGuid().ToString();
        aID.m_IDBackup = aID.m_UUID;
        m_UUIDtoObj.Add(aID.m_UUID, aID);
        m_ObjToUUID.Add(aID, aID.m_UUID);
    }
    private static void UnregisterUUID(MonoBehaviour_UUID aID)
    {
        m_UUIDtoObj.Remove(aID.m_UUID);
        m_ObjToUUID.Remove(aID);
    }
#endregion

    [SerializeField] [Readonly("UUID","Unique identifier that persists, to be able to identify objects consistently. Useful to manage consistent keys for translation")]
    private string m_UUID = null;
    private string m_IDBackup = null;

    /// <summary>
    /// This is guaranteed to be unique for this scene, and it persists when you duplicate objects, reopen editor, etc.<br></br>
    /// That is because this class uses the serialization interface to ensure the ID is unique and is serialized with the object.<br></br>
    /// This does not execute in runtime, so be extra careful with dynamically loaded objects, and <b>save the scene manually</b> for this ID to persist.
    /// </summary>
    public string ID { get { return m_UUID; } }

    /// <summary>
    /// Do not call this method manually. Let C# and Unity handle that for us.
    /// </summary>
    public void OnAfterDeserialize()
    {
        if (m_UUID == null || m_UUID != m_IDBackup)
            RegisterUUID(this);
    }
    /// <summary>
    /// Do not call this method manually. Let C# and Unity handle that for us.
    /// </summary>
    public void OnBeforeSerialize()
    {
        if (m_UUID == null || m_UUID != m_IDBackup)
            RegisterUUID(this);
    }
#if UNITY_EDITOR
    void Update()
    {
        if (Application.isPlaying)
            return;
        //if()
    }
#endif
    void OnDestroy()
    {
        UnregisterUUID(this);
        m_UUID = null;
    }
}
