﻿/*********************************
 * Created by: David Giraldo
 * Date Created: 18/03/2021
 * Last Updated by: David Giraldo
 * Last Updated: 18/03/2021
 *
 * Files handles the theme for the text,
 * This is an Example Class, please do not use this, create your own ELS_ThemeManager Class and inherit from ELS_ThemeManagerBase.
 * ******************************************/

namespace ELS_Localise
{
    public class ELS_ThemeManagerExample : ELS_ThemeManagerBase
    {

    }
}

