﻿/*********************************
 * Created by: David Giraldo
 * Date Created: 27/04/2021
 * Last Updated by: David Giraldo
 * Last Updated: 27/04/2021
 *
 * Files handles the colour theme for the images.
 * ******************************************/
using System.Collections;
using System.Collections.Generic;
using  UnityEngine.UI;

using UnityEngine;

namespace ELS_Localise
{
    public class ELS_ImageTheme : MonoBehaviour
    {
        [SerializeField] private ImageThemeColours _imageTheme;
        [SerializeField] private Image _image;
        private bool _languagedHasChanged = false;
        
        /// <summary>
        /// Get the Text Component of the Game Object.
        /// </summary>
        protected virtual void Awake()
        {
            if (!_image)
            {
                _image = GetComponent<Image>();
            }
            ELS_LocalisationBase.OnLanguageChanged -= OnLanguageChange;
            ELS_LocalisationBase.OnLanguageChanged += OnLanguageChange;
        }

        /// <summary>
        /// Used to change the colours of the images.
        /// </summary>
        /// <param name="language"></param>
        /// <param name="fontDetails"></param>
        /// <param name="themeColours"></param>
        protected virtual void OnLanguageChange(LanguageDetails language, List<FontDetails> fontDetails,
            MainThemeDetails themeColours)
        {
            if (language == null)
            {
                return;
            }

            ChangeTheme(true);
        }

        /// <summary>
        /// Changes the image colour.
        /// </summary>
        protected virtual void ChangeTheme(bool setIsSet)
        {
            if (!ELS_LocalisationBase.Instance)
            {
                Debug.LogWarning("ELS_LocalisationBase is missing!!");
                return;
            }

            MainThemeDetails details = ELS_LocalisationBase.Instance.GetCurrentColourTheme();

            switch (_imageTheme)
            {
                case ImageThemeColours.PrimaryColour:
                    _image.color = details.PrimaryColour;
                    break;

                case ImageThemeColours.SecundaryColour:
                    _image.color = details.SecundaryColour;
                    break;

                case ImageThemeColours.ExtraColour1:
                    _image.color = details.WarnColour;
                    break;
                
                case ImageThemeColours.ExtraColour2:
                    _image.color = details.ExtraColour2;
                    break;
                default:

                    break;
            }
            
            if(setIsSet)
                _languagedHasChanged = true;
        }

        private void OnEnable()
        {
            if (_languagedHasChanged)
            {
                return;
            }

            ChangeTheme(true);
        }
    }
}
