﻿/*********************************
 * Created by: David Giraldo
 * Date Created: 12/02/2021
 * Last Updated by: David Giraldo
 * Last Updated: 06/05/2021
 *
 * Files handles the Text Components of the Project,
 * This is a base class, you must not modify this class,
 * if you need to Modify this class, create your own and inherit from this class.
 * ******************************************/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using ELS_Localise;

[DisallowMultipleComponent]
[RequireComponent(typeof(RectTransform))]
[RequireComponent(typeof(CanvasRenderer))]
[RequireComponent(typeof(TextMeshProUGUI))]
public abstract partial class ELS_TextBase : MonoBehaviour_UUID
{
    public string LocalisationKey { get { return _localisationKey; } }
    [Tooltip("Key to identify the text, this will be coming from the server")]
    [SerializeField] protected string _localisationKey; // leave blank if it is a dynamic string

    [Tooltip("This is used to reverse the text, for example the Arabic language.")]
    protected bool _isRightToLeft = false;
    public bool IsRightToLeft { get { return _isRightToLeft; } }

    [Tooltip("This is used to attach the letters together using the arabic plugic")]
    [SerializeField] protected bool _attachLettersTogether = false;

    [SerializeField] public TMP_Text _textComponent;
    [SerializeField] protected ELS_arabicFixer _arabicFixer;
    [SerializeField] [TextArea] protected string _originalText;

    protected bool _languageHasChanged = false;

    /// <summary>
    /// Set the References.
    /// </summary>
    protected virtual void OnValidate()
    {
        _textComponent = GetComponent<TMP_Text>();
        if (!_textComponent)
        {
            Debug.LogError("TMP_Text component missing!!! on game object: " + gameObject.name);
        }
        else
        {
            _originalText = _textComponent.text;
        }
    }

    /// <summary>
    /// Get the language details if it has not been set.
    /// </summary>
    protected virtual void OnEnable()
    {
        if (!_languageHasChanged && ELS_LocalisationBase.Instance)
        {
            OnLanguageChange(ELS_LocalisationBase.Instance.GetCurrentLanguage(), ELS_LocalisationBase.Instance.GetCurrentFontDetails(), ELS_LocalisationBase.Instance.GetCurrentColourTheme());
        }
    }

    /// <summary>
    /// Get the Text Component of the Game Object.
    /// </summary>
    protected virtual void Awake()
    {
        if (!_textComponent)
        {
            _textComponent = GetComponent<TMP_Text>();
        }
        else
        {
            _originalText = _textComponent.text;
        }

        ELS_LocalisationBase.OnLanguageChanged -= OnLanguageChange;
        ELS_LocalisationBase.OnLanguageChanged += OnLanguageChange;
    }

    /// <summary>
    /// Use this To Set the Text, do not reference the TMP_Text component to set the text,
    /// All Text must Pass through here!!
    /// </summary>
    /// <param name="text"></param>
    public virtual void SetText(string text)
    {
        _originalText = text;

        if (!_attachLettersTogether)
        {
            _textComponent.text = text;
        }
        else
        {
            _textComponent.text = _arabicFixer.GetFixedText(_originalText);
        }
    }

    /// <summary>
    /// Gets the current text from the text object.
    /// </summary>
    /// <returns></returns>
    public virtual string GetText()
    {
        return _textComponent.text;
    }

    /// <summary>
    /// Sets the localisation key, this is coming from the automatic localisation script. <see cref="ELS_LocaliseScene"/>
    /// </summary>
    /// <param name="key"></param>
    public virtual void SetLocalisationKey(string key)
    {
        _localisationKey = key;
        string text = GetText();
        if (!_originalText.Equals(text))
        {
            _originalText = text;
#if UNITY_EDITOR
            UnityEditor.EditorUtility.SetDirty(this);
#endif
        }
    }

    /// <summary>
    /// The event setting the language details and the theme.
    /// </summary>
    /// <param name="language"></param>
    protected virtual void OnLanguageChange(LanguageDetails language, List<FontDetails> fontDetails, MainThemeDetails themeColours)
    {
        if(language == null)
        {
            return;
        }

        if (ELS_LocalisationBase.Instance && !string.IsNullOrEmpty(_localisationKey))
        {
            string text = ELS_LocalisationBase.Instance.GetLocalisationKey(_localisationKey);
            if (text != null)
                SetText(text);
        }
        else
        {
            Debug.LogWarning("Instance null or no key null");
        }

        //Reverse the text if is not already.
        _textComponent.isRightToLeftText = _isRightToLeft && !_textComponent.isRightToLeftText && !_attachLettersTogether ? true : false;

        if (_attachLettersTogether && !string.IsNullOrEmpty(_originalText))
        {
            if(gameObject.GetComponent<ELS_arabicFixer>() == null)
            {
                gameObject.AddComponent<ELS_arabicFixer>();
            }
            _arabicFixer = GetComponent<ELS_arabicFixer>();
            SetText(_originalText);
        }

        if(fontDetails != null && fontDetails.Count > 0)
        {
            for (int i = 0; i < fontDetails.Count; i++)
            {
                SetFont(fontDetails[i].ThemeDetails, fontDetails[i].Font);
                SetColour(fontDetails[i].ThemeDetails, fontDetails[i].TextColour, fontDetails[i].TextAlternativeColour);
            }
        }
        _languageHasChanged = true;
    }

    /// <summary>
    /// Unsubcribe from any events.
    /// </summary>
    protected virtual void OnDestroy()
    {
        ELS_LocalisationBase.OnLanguageChanged -= OnLanguageChange;
    }
}
