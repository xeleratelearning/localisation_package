﻿/*********************************
 * Created by: David Giraldo
 * Date Created: 11/03/2021
 * Last Updated by: David Giraldo
 * Last Updated: 18/03/2021
 *
 * Files handles the Localisation for the apps,
 * This is an Example Class, please do not use this, create your own ELS_Localisation Class and inherit from ELS_LocalisationBase.
 * ******************************************/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ELS_Localise
{
    [RequireComponent(typeof(ELS_ThemeManagerExample))]
    public class ELS_LocalisationExample : ELS_LocalisationBase
    {
        protected new static ELS_LocalisationExample _instance;
        public new static ELS_LocalisationBase Instance { get { return _instance; } }
    }
}
