﻿/*********************************
 * Created by: David Giraldo
 * Date Created: 18/03/2021
 * Last Updated by: David Giraldo
 * Last Updated: 18/03/2021
 *
 * Files handles the theme for the text.
 * ******************************************/
using UnityEngine;
using TMPro;
using ELS_Localise;

public partial class ELS_TextBase
{
    [SerializeField] private ThemeTextDetails _textDetails = ThemeTextDetails.Content;
    [SerializeField] private bool _alternativeTextColour = false;

    /// <summary>
    /// Set the Font Asset.
    /// </summary>
    /// <param name="font"></param>
    public virtual void SetFont(ThemeTextDetails textDetails, TMP_FontAsset font)
    {
        if(_textDetails == textDetails)
        {
            _textComponent.font = font;
        }
    }

    /// <summary>
    /// Set the Font Asset, this will force to use it.
    /// </summary>
    /// <param name="font"></param>
    public virtual void SetFont(TMP_FontAsset font)
    {
        _textComponent.font = font;
    }

    /// <summary>
    /// Set the colour of the text.
    /// </summary>
    /// <param name="font"></param>
    public virtual void SetColour(ThemeTextDetails textDetails, Color32 colour, Color32 alternativeColour)
    {
        if (_textDetails == textDetails && !_alternativeTextColour)
        {
            _textComponent.color = colour;
        }
        else if (_textDetails == textDetails && _alternativeTextColour)
        {
            _textComponent.color = alternativeColour;
        }
    }

    /// <summary>
    /// Set the colour of the text, forced.
    /// </summary>
    /// <param name="font"></param>
    public virtual void SetColour(Color32 colour)
    {
        _textComponent.color = colour;
    }
}

