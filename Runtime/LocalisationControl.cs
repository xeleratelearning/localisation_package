/*********************************
 * Created by: David Giraldo
 * Date Created: 06/05/2021
 * Last Updated by: David Giraldo
 * Last Updated: 20/05/2021
 *
 * Files handles the Setting of a language, pass the app uuid to get the languages for the app.
 * ******************************************/
using ELS_Localise;
using UnityEngine;

namespace ELS_Localise
{
    public class LocalisationControl : MonoBehaviour
    {
        [SerializeField] private string _uuid;

        private void Start()
        {
            if (ELS_LocalisationBase.Instance && !string.IsNullOrEmpty(_uuid))
            {
                ELS_LocalisationBase.Instance.GetAppLocalisationJson(_uuid);
            }
        }
    }
}
