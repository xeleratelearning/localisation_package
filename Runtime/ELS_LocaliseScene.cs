﻿/*********************************
 * Created by: David Giraldo
 * Date Created: 18/03/2021
 * Last Updated by: Paulo Renan
 * Last Updated: 14/07/2021, 10:58
 *
 * Handles the Logic for Localising the game.
 * 1. Create a CSV file for each scene. Scene name must be unique.
 * 2. Translate them into languages adding language to file name (myTranslatedCSV.en.csv, myTranslatedCSV.pt.csv)
 * 3. Generate json from the CSVs, and upload it to the server.
 * ******************************************/
using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using ELS_Localise;

#if UNITY_EDITOR
using UnityEditor;
using UnityEditor.SceneManagement;

[ExecuteInEditMode]
#endif
public class ELS_LocaliseScene : MonoBehaviour
{
    private static ELS_LocaliseScene _instance;
    public static ELS_LocaliseScene Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<ELS_LocaliseScene>();
            }
            return _instance;
        }
    }

    [Header("Give the keys a specific name")]
    [SerializeField] private string _sceneName = "";
#if UNITY_EDITOR
    /// <summary>
    /// MonoScript is an editor tool. You can assign a c# script on the inspector, and the file will be interpreted as a class to add the component ELS_Text that may sit in the project. This way we avoid having to use System.Reflection
    /// </summary>
    [Comment("Your ELS_Text c# script, subclass of ELS_TextBase\nUse this if you have one in the project")]
    [SerializeField, SerializeReference] private MonoScript ELS_TextClass;
#endif

    private string KeyPrefix => "_key_" + _sceneName + "_";

    public string SceneName { get => _sceneName; set => _sceneName = value; }

    private bool markSceneDirty = false;
    private List<string> _lines = new List<string>();

    public static readonly Languages[] RTL_Languages = new Languages[] { // from https://meta.wikimedia.org/wiki/Template:List_of_language_names_ordered_by_code
        Languages.ar,
        Languages.ar_AE,
        Languages.ar_AE,
        Languages.ar_BH,
        Languages.ar_DZ,
        Languages.ar_EG,
        Languages.ar_IQ,
        Languages.ar_JO,
        Languages.ar_KW,
        Languages.ar_LB,
        Languages.ar_LY,
        Languages.ar_MA,
        Languages.ar_OM,
        Languages.ar_QA,
        Languages.ar_SA,
        Languages.ar_SY,
        Languages.ar_TN,
        Languages.ar_YE,
        //Languages.arc,
        //Languages.ckb,
        Languages.dv,
        Languages.fa,
        Languages.fa_IR,
        Languages.ha,
        Languages.he,
        Languages.he_IL,
        //Languages.khw,
        Languages.ks,
        Languages.ps,
        Languages.ur,
        Languages.ur_PK,
        Languages.uz,
        Languages.cy,
        Languages.Cy_uz_UZ, // is that supposed to be a variation of uzbeki? or perhaps some lost Welsh (cy) in Uzbekistan? most likely a typo, but let's add to RTL just in case
        Languages.Lt_uz_UZ, // is that supposed to be a variation of uzbeki? or perhaps some lost Lithuanian (lt) in Uzbekistan? most likely a typo, but let's add to RTL just in case
        Languages.yi,
    };
    private static string GetLangDirection(Languages lang)
    {
        if (RTL_Languages.Contains(lang)) return "rtl";
        return "ltr";
    }


#if UNITY_EDITOR // implementation meant for the editor only

    [MenuItem("Tools/Localization/Convert CSVs into json")]
    public static void ConvertCSVsToJson()
    {
        LanguagesAvailable result = new LanguagesAvailable();
        result.languages = new List<LanguageDetails>();
        var splitCSVsWith = new char[] { ',' };
        foreach (var file in Directory.EnumerateFiles(Application.persistentDataPath, "*.csv")) // list all csv files in folder
        {
            if (string.IsNullOrEmpty(file)) continue;
            var lines = File.ReadAllLines(file, Encoding.UTF8); // read the csv file
            if (lines == null) continue;
            
            var fileNameChunks = file.Split('.');
            if (fileNameChunks == null || fileNameChunks.Length < 2) continue; // filename doesn't have the expected naming format 'myfile.<language>.csv' where <language> is an ELS_Localise.Languages enum
            string langFromFile = fileNameChunks[fileNameChunks.Length - 2]; // second last chunk from file. Example: 'en' from 'myfile.en.csv'
            Languages parsedLang;
            if(Enum.TryParse(langFromFile, out parsedLang))
            {
                Debug.Log("Processing " + file);
                var langDetails = result.languages.FirstOrDefault(x => x.language.Equals(langFromFile));
                bool newLang = false;
                if (langDetails == null) // I haven't made an entry with this language yet
                {
                    langDetails = new LanguageDetails() { direction = GetLangDirection(parsedLang), join = false, voice_over = new List<VoiceOver>(), translation = new List<Translation>() };
                    langDetails.language = langFromFile;
                    newLang = true;
                }
                foreach (var line in lines)
                {
                    if (string.IsNullOrEmpty(line)) continue;
                    var keyText = line.Split(splitCSVsWith, 3, StringSplitOptions.None); // we only care about the split between key (column A, index 0) and text (column C, index 2). In previous versions of this package, text was column B, index 1.
                    if (keyText == null || keyText.Length < 3) continue;
                    langDetails.translation.Add(new Translation() { key = keyText[0], text = keyText[2].Trim().RemoveDoubleQuotes().RemoveBothEndingsQuotes() });
                }
                if (newLang) result.languages.Add(langDetails);
            }
        }

        StartFreshFile(Application.persistentDataPath, Application.productName + "_translations_v" + Application.version + ".json", JsonUtility.ToJson(result, true));
    }
    public static void GrabNameFromScene()
    {
        string name = EditorSceneManager.GetActiveScene().name;
        bool isNameDifferent = !Instance.SceneName.Equals(name);
        Instance.SceneName = name;
        if (isNameDifferent)
        {
            EditorSceneManager.MarkSceneDirty(EditorSceneManager.GetActiveScene());
            EditorUtility.SetDirty(Instance);
        }
    }

    [MenuItem("Tools/Localization/Generate File and Add Components")]
    // Go through all the objects in the scene.
    public static void GenerateFile()
    {
        Instance._lines = new List<string>();
        Instance.markSceneDirty = false;

        // list with all game objects in the scene
        List<Transform> trans = new List<Transform>();
        var roots = EditorSceneManager.GetActiveScene().GetRootGameObjects();
        foreach (var blood in roots) // sepultura
        {
            trans = trans.Concat(blood.GetComponentsInChildren<Transform>(true)).ToList();
            trans = trans.Concat(blood.GetComponentsInChildren<RectTransform>(true).Where(x => !trans.Contains(x))).ToList();
        }

        // find out if the type assigned is a valid subclass of ELS_TextBase
        Type customType = null;
        if (Instance.ELS_TextClass != null)
        {
            var _customType = Instance.ELS_TextClass.GetClass();
            if (_customType.IsSubclassOf(typeof(ELS_TextBase)))
            {
                Debug.Log(_customType + " is subclass of ELS_TextBase");
                customType = _customType;
            }
            else
            {
                Debug.LogWarning(_customType + " is not a valid class. Please assign a class derived from ELS_TextBase. See ELS_TextExample.");
            }
        }
        if (customType == null)
        {
            Debug.Log("No custom subclass assigned. Using ELS_TextExample");
        }

        // add the text components where appropriate
        foreach (var item in trans)
        {
            Instance.AddToList<ELS_TextExample>(item.gameObject, customType);
        }

        Instance.SaveFile();
        if (Instance.markSceneDirty)
            EditorSceneManager.MarkSceneDirty(EditorSceneManager.GetActiveScene());
    }

    /// <summary>
    /// Gets each game object and adds the localisation scripts.
    /// </summary>
    /// <param name="myGameObject"></param>
    private void AddToList<T>(GameObject myGameObject, Type customType = null) where T : ELS_TextBase
    {
        if (myGameObject == null) return;
        var myTMP_Text = myGameObject.GetComponent<TMP_Text>();
        if (myTMP_Text != null && !string.IsNullOrEmpty(myTMP_Text.text))
        {
            int amount;
            if (int.TryParse(myTMP_Text.text, out amount))
            {
                return; // value is a number, so skip it
            }
            string textKey = null;
            var els_text_component = customType != null ? myGameObject.GetComponent(customType) : myGameObject.GetComponent<T>();
            if (els_text_component == null) // gameobject doesn't yet have a els_text component yet
            {
                var myTMP_Pro = myGameObject.GetComponent<TextMeshPro>();
                if (myTMP_Pro != null) // this gameobject has a TextMeshPro that is not in a canvas. Replace that with a TextMeshProUGUI (the one that should be in a canvas)
                {
                    Debug.LogWarning("<size=20>REPLACING COMPONENT</size>: " + myTMP_Pro.gameObject.name + "\n Make sure this belongs to a canvas, and revise its TMP properties (color, font, size, etc)");
                    string t = myTMP_Pro.text; // save the text in a temp string
                    if (Application.isPlaying) // that's the correct way to destroy when using a script that can run in edit mode
                        Destroy(myTMP_Pro);
                    else
                        DestroyImmediate(myTMP_Pro);
                    var newComponent = myGameObject.AddComponent<TextMeshProUGUI>(); // add the component
                    newComponent.text = t; // assign the text back to the new component
                }
                if (customType != null)
                    els_text_component = myGameObject.AddComponent(customType); // add the els_text custom component
                else
                    els_text_component = myGameObject.AddComponent<T>(); // add the els_text component
                markSceneDirty = true;
                Debug.Log(myGameObject.name + " added els text component");
            }
            textKey = (els_text_component as ELS_TextBase).ID; // identifier that is guaranteed to be unique for this scene
            if (string.IsNullOrEmpty(textKey))
            {
                Debug.LogError("Something went wrong on " + myGameObject.name + ", the UUID should not be empty");
#if UNITY_EDITOR
                UnityEditor.EditorGUIUtility.PingObject(myGameObject);
#endif
                return;
            }
            textKey = textKey.ReplaceSpecialCharacters("_");
            textKey = textKey.ReplaceLineBreaks("_");

            string text = "";

            text = myTMP_Text.text;
            text = text.DuplicateDoubleQuotes();// single quotes mess up the csv, we gotta use double quotes for that

            if (text.Contains(",")) // comma is the default delimiter for csv, so when the text has comma, it should be wrapped in quote marks
            {
                text = "\"" + text + "\"";
            }
            text = text.ReplaceLineBreaks("<br>");
            //Debug.Log(text);

            var tb = myGameObject.GetComponent<ELS_TextBase>();
            if (!markSceneDirty && !tb.LocalisationKey.Equals(KeyPrefix + textKey))// if at least one key was different
            {
                markSceneDirty = true; // mark scene as dirty (need to save it). That is because Unity can't tell that we changed something.
                Debug.Log(myGameObject.name + " changed localisation key");
            }

            if (customType != null)
                (myGameObject.GetComponent(customType) as ELS_TextBase).SetLocalisationKey(KeyPrefix + textKey);
            else
                myGameObject.GetComponent<T>().SetLocalisationKey(KeyPrefix + textKey);

            _lines.Add(KeyPrefix + textKey + "," + myGameObject.name.ReplaceSpecialCharacters("_") + "," + text);
        }
    }

    /// <summary>
    /// Saves the file to disk.
    /// </summary>
    private void SaveFile()
    {
        if (_lines == null || _lines.Count == 0)
        {
            Debug.Log("No text to translate on this scene");
            return;
        }
        string filename = "v" + Application.version + "_" + _sceneName + "_Localization.csv";
        StartFreshFile(Application.persistentDataPath, filename);
        if(WriteLines(Application.persistentDataPath, filename, _lines))
            Debug.LogError("file Saved to: " + Path.Combine(Application.persistentDataPath, filename));
    }

    /// <summary>
    /// Check directory before storing file.
    /// </summary>
    /// <param name="path"></param>
    /// <param name="fileName"></param>
    /// <param name="data">each line is a string</param>
    /// <returns></returns>
    public static bool WriteLines(string path, string fileName, List<string> data, Encoding encoding = null)
    {
        bool retValue = false;
        try
        {
            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);
            File.AppendAllLines(Path.Combine(path, fileName), @data, encoding ?? Encoding.UTF8); // this takes care of using the correct line break, so we don't need to concatenate \n or \r to the string manually
            retValue = true;
        }
        catch (System.Exception ex)
        {
            string ErrorMessages = "File Write Error\n" + ex.Message;
            retValue = false;
            Debug.LogError(ErrorMessages);
        }
        return retValue;
    }
    public static bool StartFreshFile(string path, string fileName, string data = "", Encoding encoding = null)
    {
        bool retValue = false;
        try
        {
            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);
            File.WriteAllText(Path.Combine(path, fileName), data, encoding ?? Encoding.UTF8);
            retValue = true;
        }
        catch (System.Exception ex)
        {
            string ErrorMessages = "File Write Error\n" + ex.Message;
            retValue = false;
            Debug.LogError(ErrorMessages);
        }
        return retValue;
    }
#endif

}