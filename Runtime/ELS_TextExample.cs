﻿/*********************************
 * Created by: David Giraldo
 * Date Created: 12/02/2021
 * Last Updated by: David Giraldo
 * Last Updated: 17/03/2021
 *
 * Files handles the Text Components of the Project, this is an example class, do
 * not use this, make another class named ELS_Text and inherit from ELS_TextBase.
 * ******************************************/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ELS_TextExample : ELS_TextBase
{

}
