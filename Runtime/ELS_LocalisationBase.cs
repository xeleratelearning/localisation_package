﻿/*********************************
 * Created by: David Giraldo
 * Date Created: 11/03/2021
 * Last Updated by: Paulo Renan
 * Last Updated: 12/07/2021, 12:01
 *
 * Files handles the Localisation for the apps.
 * Do not modify this class in any way, if you need to change
 * something use ELS_Localisation.
 * ******************************************/
using UnityEngine;
using System;
using System.Collections.Generic;
using System.Collections;
using ELS_ContentDownload;
using  ELS_Authenticate;
using TMPro;
using UnityEngine.Networking;

namespace ELS_Localise
{
    public abstract class ELS_LocalisationBase : MonoBehaviour
    {
        protected static ELS_LocalisationBase _instance;
        public static ELS_LocalisationBase Instance { get { return _instance; } }

        public static Action<LanguageDetails, List<FontDetails>, MainThemeDetails> OnLanguageChanged;

        public LanguagesAvailable LanguagesFromServer { get { return _languagesFromServer; } }
        [Header("The Languages available for this app.")]
        [SerializeField] protected LanguagesAvailable _languagesFromServer; //This will be populated from the server.

        public string UUID { get { return _uuID; } }
        [Header("This is the id for the translation, Unique for each app")]
        [SerializeField] protected string _uuID = "";
        
        [SerializeField] private Languages _currentLanguage = Languages.en_GB;

        [Header("The Latest Languages Json from the server")]
        [TextArea] [SerializeField] protected string _currentLanguagesJson; // This is to store the latest languages Json in case there is no internent connection.

        [SerializeField] protected ELS_ThemeManagerBase _themeDetails;

        [SerializeField] private bool _getTokenOnStart = true;

        [Tooltip("Type here the full path of a translation json you want to test before uploading to the server.\nAlternatively, you can paste the content of the json file, but Unity might not like very long strings here and might cut your content.")]
        [Header("Editor Test Override")][SerializeField][TextArea] private string _testTranslationJson;

        /// <summary>
        /// Get the references just in case.
        /// </summary>
        private void OnValidate()
        {
            if(!_themeDetails)
            {
                _themeDetails = GetComponent<ELS_ThemeManagerBase>();
            }
        }

        /// <summary>
        /// Make a singleton class of this object.
        /// </summary>
        protected virtual void Awake()
        {
            if (_instance != null && _instance != this)
            {
                Debug.LogWarning("Instance already Exists, destroying game Obejct");
                Destroy(gameObject);
                return;
            }
            else
            {
                _instance = this;
                DontDestroyOnLoad(gameObject);
            }
            if (!_themeDetails)
            {
                _themeDetails = GetComponent<ELS_ThemeManagerBase>();
            }

            LoadStoredJson();

            ELS_AuthenticationBase.OnUserLogIn -= SetThemeOnLogIn;
            ELS_AuthenticationBase.OnUserLogIn += SetThemeOnLogIn;

        }

        protected void OnDestroy()
        {
            ELS_AuthenticationBase.OnUserLogIn -= SetThemeOnLogIn;

        }

        /// <summary>
        /// Get the languages on Start.
        /// </summary>
        protected virtual void Start()
        {
            if(_getTokenOnStart)
            {
                GetAppLocalisationJson();
            }
        }

        /// <summary>
        /// This will load the latest stored json from the server.
        /// </summary>
        protected virtual void LoadStoredJson()
        {
            if (PlayerPrefs.HasKey("_currentLanguagesJson"))
            {
                _currentLanguagesJson = PlayerPrefs.GetString("_currentLanguagesJson");
#if UNITY_EDITOR
                if (string.IsNullOrEmpty(_testTranslationJson)) return;

                if(_testTranslationJson.StartsWith("{"))
                    _currentLanguagesJson = _testTranslationJson;
                else
                    _currentLanguagesJson = System.IO.File.ReadAllText(_testTranslationJson, System.Text.Encoding.UTF8);
#endif
            }
        }

        /// <summary>
        /// Make the API call to get the available languages for the app, leave uuid null to use the UUID from this class.
        /// </summary>
        public virtual void GetAppLocalisationJson(string uuid = null)
        {
            if (uuid == null)
            {
                uuid = _uuID;
            }
            else
            {
                _uuID = uuid;
            }
            
            if(ELS_ContentDownloadBase.Instance && !string.IsNullOrEmpty((uuid)))
            {
                ELS_ContentDownloadBase.Instance.GetAssetBundlesFromApp(_uuID, ONAssetBundleCallback);
            }
            else
            {
                Debug.LogError("Instance null or app uuid is null");
            }
        }

        /// <summary>
        /// The call bacl from the content download api.
        /// </summary>
        /// <param name="obj"></param>
        private void ONAssetBundleCallback(AssetBundleRoot obj)
        {
            if (string.IsNullOrEmpty(obj.assets.translations))
            {
                Debug.LogError("No Translations found to download");
#if UNITY_EDITOR
                if(!string.IsNullOrEmpty(_testTranslationJson))
                    OnLocalisationCallBack(_testTranslationJson, false);
#endif
            }
            else
            {
                StartCoroutine(GetRequest(obj.assets.translations, OnLocalisationCallBack));
            }
        }
        
        /// <summary>
        /// This is used to get the Json localisation for the app.
        /// </summary>
        /// <param name="uri"></param>
        /// <param name="onJsonCallback"></param>
        /// <returns></returns>
        protected  virtual IEnumerator GetRequest(string uri, Action<string, bool> onJsonCallback)
        {
            using (UnityWebRequest webRequest = UnityWebRequest.Get(uri))
            {
                // Request and wait for the desired page.
                yield return webRequest.SendWebRequest();

                if (webRequest.error == null)
                {
                    onJsonCallback.Invoke(webRequest.downloadHandler.text, false);
                }
                else
                {
                    onJsonCallback.Invoke(webRequest.error, true);
                }
                
                webRequest.Dispose();
            }
        }

        /// <summary>
        /// The call back from the API call to get all the languages.
        /// </summary>
        /// <param name="Json"></param>
        /// <param name="failed"></param>
        protected virtual void OnLocalisationCallBack(string Json, bool failed)
        {
            //Debug.Log(Json);
#if UNITY_EDITOR
            if (!string.IsNullOrEmpty(_testTranslationJson))
            {
                if (_testTranslationJson.StartsWith("{"))
                    Json = _testTranslationJson;
                else
                    Json = System.IO.File.ReadAllText(_testTranslationJson, System.Text.Encoding.UTF8);
            }

#endif
            Json = Json.EnsureItStartsWith('{');

            if (!failed)
            {
                PlayerPrefs.SetString("_currentLanguagesJson", Json); //save the latest Json.
            }
            else
            {
                Debug.Log(Json);
                if (!string.IsNullOrEmpty(_currentLanguagesJson))
                {
                    Json = _currentLanguagesJson;
                }
                else
                {
                    Debug.LogError("Error on API call, there is no stored json too.");
                    return;
                }
            }
            
            try
            {
                _languagesFromServer = JsonUtility.FromJson<LanguagesAvailable>(Json);
                FireLanguageChange();
            }
            catch (Exception e)
            {
              Debug.LogError("Error With Json => " + e);
                return;
            }
        }

        /// <summary>
        /// Set the Font on the asset bundle callback.
        /// </summary>
        /// <param name="obj"></param>
        private void ONAssetBundleCallback(AssetBundle obj)
        {
            TMP_FontAsset font = obj.LoadAsset<TMP_FontAsset>("font");
            
            if (font)
            {
                _themeDetails.SetFont(font);
            }
            
            FireLanguageChange();
        }

        protected virtual void SetThemeOnLogIn(bool firstime = false)
        {
            if (ELS_AuthenticationBase.Instance.CurrentSessionData.company.app_customisation != null && !string.IsNullOrEmpty(ELS_AuthenticationBase.Instance.CurrentSessionData.company.app_customisation
                .font_primary_color))
            {
                Color primary = Color.white;
                ColorUtility.TryParseHtmlString(ELS_AuthenticationBase.Instance.CurrentSessionData
                    .company.app_customisation
                    .font_primary_color, out primary);
                
                Color secundary = Color.black;
                ColorUtility.TryParseHtmlString(ELS_AuthenticationBase.Instance.CurrentSessionData
                    .company.app_customisation
                    .font_secondary_color, out secundary);

                _themeDetails.SetFontColours(primary, secundary, Color.white, Color.black);
                
                Color primary2 = Color.white;
                ColorUtility.TryParseHtmlString(ELS_AuthenticationBase.Instance.CurrentSessionData
                    .company.app_customisation
                    .primary_color, out primary2);

                Color secundary2 = Color.white;
                ColorUtility.TryParseHtmlString(ELS_AuthenticationBase.Instance.CurrentSessionData
                    .company.app_customisation
                    .secondary_color, out secundary2);

                Color warnColor = Color.white;
                ColorUtility.TryParseHtmlString(ELS_AuthenticationBase.Instance.CurrentSessionData
                    .company.app_customisation
                    .warn_color, out warnColor);

                _themeDetails.SetMainThemeColors(primary2, secundary2, warnColor, Color.black);
            }
            
            if (ELS_AuthenticationBase.Instance.CurrentSessionData.company.app_customisation != null && !string.IsNullOrEmpty(ELS_AuthenticationBase.Instance.CurrentSessionData.company.app_customisation
                .font))
            {
                ELS_ContentDownloadBase.Instance.GetAssetBundle(ELS_AuthenticationBase.Instance.CurrentSessionData.company.app_customisation
                    .font, ONAssetBundleCallback);
            }
            else
            {
                FireLanguageChange();
            }
        }

        /// <summary>
        /// Changes the language and theme details.
        /// </summary>
        protected virtual void FireLanguageChange()
        {
            for (int i = 0; i < _languagesFromServer.languages.Count; i++)
            {
                if (_languagesFromServer.languages[i].language == _currentLanguage.ToString() || _languagesFromServer.languages[i].language.Replace("-", "_") == _currentLanguage.ToString())
                {
                    if (OnLanguageChanged != null)
                    {
                        Debug.Log("LanguageChanged to " + _currentLanguage.ToString());
                        OnLanguageChanged.Invoke(_languagesFromServer.languages[i], _themeDetails.GetFontDetails(), 
                            _themeDetails.GetThemeColours());
                    }
                    break;
                }
            }
        }

        /// <summary>
        /// Returns the text of a given key.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public string GetLocalisationText(string key)
        {
            for (int i = 0; i < _languagesFromServer.languages.Count; i++)
            {
                if (_languagesFromServer.languages[i].language == _currentLanguage.ToString() || _languagesFromServer.languages[i].language.Replace("-", "_") == _currentLanguage.ToString())
                {
                    for (int j = 0; j < _languagesFromServer.languages[i].translation.Count; j++)
                    {
                        if (_languagesFromServer.languages[i].translation[j].key == key)
                        {
                            return _languagesFromServer.languages[i].translation[j].text;
                        }
                    }
                }
            }
            
            Debug.LogError("No Key Found! => " + key);
            return key;
        }

        /// <summary>
        /// Call this to change the language.
        /// </summary>
        /// <param name="language"></param>
        public virtual void ChangeLanguage(string language)
        {
            for (int i = 0; i < _languagesFromServer.languages.Count; i++)
            {
                if (_languagesFromServer.languages[i].language == language)
                {
                    _currentLanguage = (Languages)Enum.Parse(typeof(Languages), language);

                    OnLanguageChanged?.Invoke(_languagesFromServer.languages[i], _themeDetails.GetFontDetails(), 
                        _themeDetails.GetThemeColours());

                    break;
                }
            }
        }

        /// <summary>
        /// Call this to get the current language in use.
        /// </summary>
        /// <returns></returns>
        public virtual LanguageDetails GetCurrentLanguage()
        {
            for (int i = 0; i < _languagesFromServer.languages.Count; i++)
            {
                if (_languagesFromServer.languages[i].language == _currentLanguage.ToString())
                {
                    return _languagesFromServer.languages[i];
                }
            }
            return null;
        }

        /// <summary>
        /// Call this to get the current font Details.
        /// </summary>
        /// <returns></returns>
        public virtual List<FontDetails> GetCurrentFontDetails()
        {
            return _themeDetails.GetFontDetails();
        }
        
        /// <summary>
        /// Call this to get the current theme Details.
        /// </summary>
        /// <returns></returns>
        public virtual MainThemeDetails GetCurrentColourTheme()
        {
            return _themeDetails.GetThemeColours();
        }

        /// <summary>
        /// Gets a key from the languages.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public virtual string GetLocalisationKey(string key)
        {
            for (int i = 0; i < _languagesFromServer.languages.Count; i++)
            {
                if (_languagesFromServer.languages[i].language == _currentLanguage.ToString())
                {
                    for (int j = 0; j < _languagesFromServer.languages[i].translation.Count; j++)
                    {
                        if(_languagesFromServer.languages[i].translation[j].key == key)
                        {
                            return _languagesFromServer.languages[i].translation[j].text;
                        }
                    }
                }
            }

            Debug.LogError("No localisation key found! " + key);
            return null;
        }
    }
}
