﻿/*********************************
 * Created by: David Giraldo
 * Date Created: 18/03/2021
 * Last Updated by: Rene Barrow
 * Last Updated: 27/11/2023
 *
 * Files handles the theme for the text,
 * This is a base class, you must not modify this class,
 * if you need to Modify this class, create your own and inherit from this class.
 * ******************************************/

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace ELS_Localise
{
    public abstract class ELS_ThemeManagerBase : MonoBehaviour
    {
        [SerializeField] protected LanguageThemeDetails _languageThemeDetails;

        private void Awake()
        {
            
        }

        /// <summary>
        /// Gets the theme for the 
        /// </summary>
        /// <param name="language"></param>
        /// <returns></returns>
        public virtual List<FontDetails> GetFontDetails()
        {
            return _languageThemeDetails.FontsDetail;
        }

        /// <summary>
        /// Sets the Colours for the Font.
        /// </summary>
        /// <param name="fontPrimary"></param>
        /// <param name="fontSecundary"></param>
        /// <param name="fontExtra1"></param>
        /// <param name="fontExtra2"></param>
        public virtual void SetFontColours(Color32 fontPrimary, Color32 fontSecundary, Color32 fontExtra1,
            Color32 fontExtra2)
        {
            AddFontColourDetail(ThemeTextDetails.Title, fontPrimary, fontSecundary);
            AddFontColourDetail(ThemeTextDetails.SubTitle, fontPrimary, fontSecundary);
            AddFontColourDetail(ThemeTextDetails.Content, fontPrimary, fontSecundary);
            AddFontColourDetail(ThemeTextDetails.ExtraContent, fontExtra1, fontExtra2);
        }

        /// <summary>
        /// Adds the details for each font.
        /// </summary>
        /// <param name="type"></param>
        /// <param name="fontPrimary"></param>
        /// <param name="fontSecundary"></param>
        protected virtual void AddFontColourDetail(ThemeTextDetails type, Color32 fontPrimary, Color32 fontSecundary)
        {
            FontDetails fontDetails = new FontDetails();
            fontDetails.ThemeDetails = type;
            fontDetails.TextColour = fontPrimary;
            fontDetails.TextAlternativeColour = fontSecundary;
            
            _languageThemeDetails.FontsDetail.Add(fontDetails);
        }

        /// <summary>
        /// Sets the main colours for the app.
        /// </summary>
        /// <param name="primary"></param>
        /// <param name="secundary"></param>
        /// <param name="extra1"></param>
        /// <param name="extra2"></param>
        public virtual void SetMainThemeColors(Color primary, Color secundary, Color extra1, Color extra2)
        {
            _languageThemeDetails.MainThemeColors.PrimaryColour = primary;
            _languageThemeDetails.MainThemeColors.SecundaryColour = secundary;
            _languageThemeDetails.MainThemeColors.WarnColour = extra1;
            _languageThemeDetails.MainThemeColors.ExtraColour2 = extra2;

        }

        /// <summary>
        /// Sets the font for the app.
        /// </summary>
        /// <param name="font"></param>
        public virtual void SetFont(TMP_FontAsset font)
        {
            for (int i = 0; i < _languageThemeDetails.FontsDetail.Count; i++)
            {
                _languageThemeDetails.FontsDetail[i].Font = font;
            }
        }

        /// <summary>
		/// This will return the colour guide for each language.
		/// </summary>
		/// <param name="language"></param>
		/// <returns></returns>
        public virtual MainThemeDetails GetThemeColours()
        {
            return _languageThemeDetails.MainThemeColors;
        }

    }
}
