/*********************************
 * Created by: David Giraldo
 * Date Created: 07/05/2021
 * Last Updated by: Rene Barrow
 * Last Updated: 27/11/2023
 *
 * Files handles the Setting up of the Logo of the company.
 * ******************************************/

using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using  ELS_Authenticate;
using  UnityEngine.Networking;
using ELS_ContentDownload;

namespace ELS_Localise
{
    public class ELS_LogoGrabber : MonoBehaviour
    {
        [SerializeField] private RawImage _image;
        private float _fixedHeight = 465;

        /// <summary>
        /// GEts the image reference.
        /// </summary>
        private void Awake()
        {
            if (!_image)
            {
                _image = GetComponent<RawImage>();
            }
        }

        private IEnumerator Start()
        {

            if (!ELS_AuthenticationBase.Instance)
            {
                Debug.LogWarning("Instance Null");
                yield break;
            }

            if (string.IsNullOrEmpty(ELS_AuthenticationBase.Instance.CurrentSessionData.authorisation_token))
            {
                Debug.LogWarning("toke Null, waiting");
                while (string.IsNullOrEmpty(ELS_AuthenticationBase.Instance.CurrentSessionData.authorisation_token))
                {
                    yield return new WaitForSeconds(1);
                }
            }
            
            if (!string.IsNullOrEmpty(ELS_AuthenticationBase.Instance.CurrentSessionData.company.logo_read_url.primary))
            {
                StartCoroutine(DownloadImage(ELS_AuthenticationBase.Instance.CurrentSessionData.company.logo_read_url.primary));
            }
            else
            {
                Debug.LogWarning("Logo URL is null");

            }

            
        }

        /// <summary>
        /// Downloads the image.
        /// </summary>
        /// <param name="MediaUrl"></param>
        /// <returns></returns>
        IEnumerator DownloadImage(string MediaUrl)
        {   
            UnityWebRequest request = UnityWebRequestTexture.GetTexture(MediaUrl);
            yield return request.SendWebRequest();
            if(request.isNetworkError || request.isHttpError) 
                Debug.LogError(request.error);
            else
                _image.texture = ((DownloadHandlerTexture) request.downloadHandler).texture;
            
            if (gameObject.name == "MenuLogo")
            {
                SizeToParent(_image);
            }
        }

        public Vector2 SizeToParent(RawImage image, float padding = 0) //float padding = 0)
        {
            float w = 0, h = 0; //_fixedHeight;
            var parent = image.GetComponentInParent<RectTransform>();
            var imageTransform = image.GetComponent<RectTransform>();

            // check if there is something to do
            if (image.texture != null)
            {
                if (!parent) { return imageTransform.sizeDelta; } //if we don't have a parent, just return our current width;
                padding = 1 - padding;
                float ratio = image.texture.width / (float)image.texture.height;
                var bounds = new Rect(0, 0, parent.rect.width, parent.rect.height);
                if (Mathf.RoundToInt(imageTransform.eulerAngles.z) % 180 == 90)
                {
                    //Invert the bounds if the image is rotated
                    bounds.size = new Vector2(bounds.height, bounds.width);
                }
                //Size by height first
                h = bounds.height * padding;
                w = h * ratio;
                if (w > bounds.width * padding)
                { //If it doesn't fit, fallback to width;
                    w = bounds.width * padding;
                    h = w / ratio;
                }
            }
            imageTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, w);
            imageTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, h);
            return imageTransform.sizeDelta;
        }
    }
}
