﻿/*********************************
 * Created by: David Giraldo
 * Date Created: 18/03/2021
 * Last Updated by: Paulo Renan
 * Last Updated: 15/07/2021, 13:02
 *
 * Handles the Logic for Localising the game.
 * 1. Create a CSV file for each scene. Scene name must be unique.
 * 2. Translate them into languages adding language to file name (myTranslatedCSV.en.csv, myTranslatedCSV.pt.csv)
 * 3. Generate json from the CSVs, and upload it to the server.
 * ******************************************/
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System.IO;
using System.Linq;
using ELS_Localise;
using System.Text;
using System;

#if UNITY_EDITOR
using UnityEditor;

[CustomEditor(typeof(ELS_LocaliseScene))]
public class ELS_LocaliseSceneEditor : Editor
{
    const int buttonsHeight = 24;
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        EditorGUILayout.Space();
        var defaultColor = GUI.backgroundColor;
        GUI.backgroundColor = Color.green;
        string tooltip = "This name should be unique in this project, because it will be added as a prefix to each translation key. You can type anything you want, or just click here to use the active scene name. But you must ensure this name is not repeated.";
        if (GUILayout.Button(new GUIContent("Grab name from scene", tooltip), GUILayout.Height(buttonsHeight)))
        {
            ELS_LocaliseScene.GrabNameFromScene();
        }
        EditorGUILayout.Space();
        GUI.backgroundColor = Color.yellow;
        tooltip = "This adds ELS_Text component to your TextMeshPro components in this scene, and save a csv file with all the texts in their original language: " + Path.Combine(Application.persistentDataPath, "myGeneratedFileForTranslation.csv");
        if (GUILayout.Button(new GUIContent("Generate File and Add Components", tooltip), GUILayout.Height(buttonsHeight)))
        {
            ELS_LocaliseScene.GenerateFile();
        }
        EditorGUILayout.Space();
        GUI.backgroundColor = Color.red;
        tooltip = "The json file will be ready to put on the server. Make sure your translated files in Persistant Data Path have the format <filename>.<language>.csv. Example:\n" + 
                  Path.Combine(Application.persistentDataPath, "myGeneratedFileForTranslation.en.csv") + "\n" +
                  ", myGeneratedFileForTranslation.pt.csv\n" +
                  ", myGeneratedFileForTranslation.de.csv";
        if (GUILayout.Button(new GUIContent("Convert CSVs into JSON",tooltip), GUILayout.Height(buttonsHeight)))
        {
            ELS_LocaliseScene.ConvertCSVsToJson();
        }
        GUI.backgroundColor = defaultColor;
    }
}

#region Inspector implementation of custom properties [Comment] and [Readonly]
[CustomPropertyDrawer(typeof(CommentAttribute))]
public class CommentDrawer : PropertyDrawer
{
    const int textHeight = 20;

    CommentAttribute commentAttribute { get { return (CommentAttribute)attribute; } }

    public override float GetPropertyHeight(SerializedProperty prop, GUIContent label)
    {
        if (commentAttribute.comment == null) return textHeight;
        return textHeight * (commentAttribute.comment.Split('\n').Length + 2);
    }

    public override void OnGUI(Rect position, SerializedProperty prop, GUIContent label)
    {
        position.y -= textHeight / 2f;
        EditorGUI.LabelField(position, new GUIContent(commentAttribute.comment, commentAttribute.tooltip), EditorStyles.boldLabel);
        float p = 1;
        if(commentAttribute.comment != null)
            p += commentAttribute.comment.Split('\n').Length;
        var pos = position;
        pos.height = textHeight;
        pos.center += Vector2.up * textHeight * p;
        EditorGUI.PropertyField(pos, prop);
    }
}

[CustomPropertyDrawer(typeof(ReadonlyAttribute))]
public class ReadonlyDrawer : PropertyDrawer
{
    const int textHeight = 20;

    ReadonlyAttribute readOnlyAttribute { get { return (ReadonlyAttribute)attribute; } }

    public override float GetPropertyHeight(SerializedProperty prop, GUIContent label)
    {
        if (readOnlyAttribute.comment == null) return textHeight;
        return textHeight * (readOnlyAttribute.comment.Split('\n').Length + 2);
    }

    public override void OnGUI(Rect position, SerializedProperty prop, GUIContent label)
    {
        position.y -= textHeight;
        EditorGUI.LabelField(position, new GUIContent(readOnlyAttribute.comment, readOnlyAttribute.tooltip), EditorStyles.boldLabel);
        float p = 1;
        if(readOnlyAttribute.comment != null)
            p += readOnlyAttribute.comment.Split('\n').Length;
        var pos = position;
        pos.height = textHeight;
        pos.center += Vector2.up * textHeight * p;

        switch (prop.propertyType)
        {
            case SerializedPropertyType.Integer:
                EditorGUI.LabelField(pos, prop.floatValue.ToString());
                break;
            case SerializedPropertyType.Boolean:
                EditorGUI.LabelField(pos, prop.boolValue.ToString());
                break;
            case SerializedPropertyType.Float:
                EditorGUI.LabelField(pos, prop.floatValue.ToString());
                break;
            case SerializedPropertyType.String:
                EditorGUI.LabelField(pos, prop.stringValue);
                break;
            case SerializedPropertyType.Enum:
                EditorGUI.LabelField(pos, prop.enumDisplayNames[prop.enumValueIndex]);
                break;
            case SerializedPropertyType.Generic:
            case SerializedPropertyType.Color:
            case SerializedPropertyType.ObjectReference:
            case SerializedPropertyType.LayerMask:
            case SerializedPropertyType.Vector2:
            case SerializedPropertyType.Vector3:
            case SerializedPropertyType.Vector4:
            case SerializedPropertyType.Rect:
            case SerializedPropertyType.ArraySize:
            case SerializedPropertyType.Character:
            case SerializedPropertyType.AnimationCurve:
            case SerializedPropertyType.Bounds:
            case SerializedPropertyType.Gradient:
            case SerializedPropertyType.Quaternion:
            case SerializedPropertyType.ExposedReference:
            case SerializedPropertyType.FixedBufferSize:
            case SerializedPropertyType.Vector2Int:
            case SerializedPropertyType.Vector3Int:
            case SerializedPropertyType.RectInt:
            case SerializedPropertyType.BoundsInt:
            case SerializedPropertyType.ManagedReference:
                EditorGUI.LabelField(pos, "Readonly serialization not implemented for " + prop.propertyType);
                break;
            default:
                break;
        }
    }
}
#endregion

#endif

#region Property attributes [Comment] and [Readonly]
[AttributeUsage(AttributeTargets.Field, Inherited = true, AllowMultiple = false)]
public class ReadonlyAttribute : PropertyAttribute
{
    public readonly string tooltip;
    public readonly string comment;

    public ReadonlyAttribute(string comment = null, string tooltip = null)
    {
        this.tooltip = tooltip;
        this.comment = comment;
    }
}

[AttributeUsage(AttributeTargets.Field, Inherited = true, AllowMultiple = false)]
public class CommentAttribute : PropertyAttribute
{
    public readonly string tooltip;
    public readonly string comment;

    public CommentAttribute(string comment = null, string tooltip = null)
    {
        this.tooltip = tooltip;
        this.comment = comment;
    }
}
#endregion