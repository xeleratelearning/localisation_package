﻿/* ***********************************
 * Created by: Paulo Renan
 * Created: 17/06/2021, 09:58
 * Last Updated by: Paulo Renan
 * Last Updated: 07/07/2021, 14:49
 * 
 * This class adds methods to the type string
 * **********************************/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ELS_Localise
{
    /// <summary>
    /// This class adds methods to the type <see cref="string"/><br></br>
    /// Example: <code>myString = myString.ReplaceWithLineBreaks("_");</code>
    /// </summary>
    public static class StringExtensions
    {
        /// <summary>
        /// mac / windows / linux use different combination of CRLF to express a line break. Let's replace them all with newValue
        /// </summary>
        public static string ReplaceLineBreaks(this string str, string newValue)
        {
            str = str.Replace("\r", newValue);// character return (CR)
            str = str.Replace("\n", newValue);// line feed (LF)
            return str;
        }
        /// <summary>
        /// Duplicate any double quote in a string.<br></br>
        /// CSV files with quotes as a text in a cell, require the quotes to be duplicated (Example: We ""are"" humans)
        /// </summary>
        public static string DuplicateDoubleQuotes(this string str)
        {
            return str.Replace("\"", "\"\"");
        }
        /// <summary>
        /// Remove any duplicated double quote in a string "", replacing it with \", appropriate for using within a json string<br></br>
        /// CSV files with quotes as a text in a cell, require the quotes to be duplicated (Example: We ""are"" only humans)<br></br>
        /// Json, on the other hand, does not like that. So, this replaces "" with \" (Example: We \"are\" only humans)
        /// </summary>
        public static string RemoveDoubleQuotes(this string str)
        {
            return str.Replace("\"\"", "\"");
        }
        /// <summary>
        /// Remove trailing and starting quotes, if found.<br></br>
        /// CSV files have comma as a separator. If the text in a given cell has comma, then the whole text in that cell should be in quotation marks. (Example: "We are only, humans")<br></br>
        /// Json, however, does not have that problem. So this function can be used to remove these.
        /// </summary>
        public static string RemoveBothEndingsQuotes(this string str)
        {
            if (str.Length > 3 && str.StartsWith("\"") && str.EndsWith("\""))
                return str.Substring(1, str.Length - 2);
            return str;
        }
        /// <summary>
        /// Ignore any starting character before the first 'desiredStartChar'<br></br>
        /// Use this with '{' if your string fails to parse to json. This basically converts from UTF-8 with BOM, to UTF-8, because every json starts with '{'.<br></br>
        /// Example:
        /// <code>myJsonString = myJsonString.EnsureItStartsWith('{');</code>
        /// </summary>
        public static string EnsureItStartsWith(this string str, char desiredStartChar)
        {
            int strStart = str.IndexOf(desiredStartChar);
            if (strStart > 0) return str.Substring(strStart);
            return str;
        }
        public static string ReplaceSpecialCharacters(this string str, string newValue)
        {
            str = str.Replace(" ", newValue);
            str = str.Replace(",",  newValue);
            str = str.Replace(".",  newValue);
            str = str.Replace("\"", newValue);
            str = str.Replace("\\", newValue);
            str = str.Replace("/", newValue);
            str = str.Replace("@", newValue);
            str = str.Replace("'", newValue);
            str = str.Replace("`", newValue);
            str = str.Replace("~", newValue);
            str = str.Replace("{", newValue);
            str = str.Replace("}", newValue);
            str = str.Replace("[", newValue);
            str = str.Replace("]", newValue);
            str = str.Replace("#", newValue);
            str = str.Replace("?", newValue);
            return str;
        }

        public const string lineBreak_CRLF = "\r\n";
    }
}