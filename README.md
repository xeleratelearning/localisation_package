# README # 
Rene#
This README explains what the localisation package is and how to set it up.

### What is this repository for? ###

* This package is for localasing and the app.
* 1.0.0
* RECOMMENDED!!!! Add the UPM Git Extension to your project, this will make it easier to update your packages, more info here: https://github.com/mob-sakai/UpmGitExtension#install

### How do I get set up? ###

* Add the Script ELS_LocalisationExample to a gameobject (this is an example script, create a new one named ELS_Localisation and inherit from ELS_LocalisationBase Do not use that script!). VERY IMPORTANT!! go to your project settings/script execution order and add this new class you created ELS_Localisation to have a -100.
* Set the UUID for the app, this is a unique ID for each App. 
* Set the session token, this can either come from the authentication manager once user is authenticated or hardcoded.
* Select the current language to be the default language.
* The ELS_Localisation script will make a request to the server on awake to get all the languages for the app.
* If your app is not localised yet, you can attach the scrip ELS_LocaliseScene to a game object, make sure to change the example ELS_TextExample to your own class on this script, then under tools, select localization/Generate file and add components.
* This will add your class to all the gameobjects which have the text mesh pro component on them, it will then generate a key for that text.
* You can find the csv file with all the keys under the filepath shown on the debug log.

* REMEMBER!! only use text mesh pro moving forward and do not call .text from the text mesh pro component, call it from the ELS_TextBase component ELS_TextBase().SetText.

* Create your own version of the thememanagerExample class, and set up at leats one language there, this must be the same language as the default language in the ELS_LocalisationBase .currentlanguage.

* Add the designated fonts and colours for each of the text types.

### Contribution guidelines ###

* You are welcome to write Unit tests for this plugin. 
* Do not modify the base classes at all, if you need to make a change to a base class submit a request to your lead developer.

### Who do I talk to? ###

* david.giraldo@e-learningstudios.com
* jack.paget@e-learningstudios.com