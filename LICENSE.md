e-Learningstudios Copyright  © 2021 e-Learningstudios

Licensed under the e-Learningstudios License for Unity-dependent projects. 

Unless expressly provided otherwise, the Software under this license is made available strictly on an “AS IS” BASIS WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. Please review the license for details on these and other terms and conditions.